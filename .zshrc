# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# make sure locale is there (docker)
export LANG=C.UTF-8

if [[ -n $SSH_CONNECTION ]]; then
  ZSH_THEME="wezm+"
else
  # ZSH_THEME="gallois"
  ZSH_THEME="agnoster"
fi
export DEFAULT_USER="ubuntu"
plugins=(
  git
  docker
  kubectl
)

source $ZSH/oh-my-zsh.sh


export VISUAL=vi
export EDITOR=vi
BC_ENV_ARGS=$HOME/.bcrc
export BC_ENV_ARGS

bindkey -v
bindkey '^P' up-history
bindkey '^N' down-history
bindkey "^Q" push-input
bindkey -M vicmd 'K' history-beginning-search-backward
bindkey -M viins '^r' history-incremental-search-backward
bindkey -M vicmd '^r' history-incremental-search-backward
autoload -U edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line

alias l='ls'
alias sl='ls'
alias la='ls -A'
alias ll='ls -l'
alias lh='ls -lh'
alias lt='ls -lrt'
alias lT='ls -lrta'
if type "nvim" > /dev/null; then
    alias vi='nvim'
else
    alias vi='vim'
fi
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias j=jobs
alias pd='pushd'
alias pop='popd'
alias lS='ls -lSr'
alias gvim='gvim -rv'
alias lla='ls -la'
alias h='fc -l -30'
alias ..='cd ..'
alias ...='cd .. ; cd ..'
alias v='view'
alias android-connect="mtpfs -o allow_other /media/GalaxyNexus"
alias android-disconnect="fusermount -u /media/GalaxyNexus"
alias gdh='git diff HEAD@{1}'

setopt extended_glob

# credit goes to:
# http://www.unix.com/tips-tutorials/31944-simple-date-time-calulation-bash.html
stamp2date() {
  date --utc --date "1970-01-01 ${1:0:10} sec" "+%Y-%m-%d %T"
}
date2stamp () {
  date --utc --date "$1" +%s
}

## docker cleanup
## credits: https://www.calazan.com/docker-cleanup-commands/
# Kill all running containers.
alias dockerkillall='docker kill $(docker ps -q)'

# Delete all stopped containers.
alias dockercleanc='printf "\n>>> Deleting stopped containers\n\n" && docker rm $(docker ps -a -q)'

# Delete all untagged images.
alias dockercleani='printf "\n>>> Deleting untagged images\n\n" && docker rmi $(docker images -q -f dangling=true)'

# Delete all stopped containers and untagged images.
alias dockerclean='dockercleanc || true && dockercleani'

alias dockerip="docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'"

export GPG_TTY="$(tty)"
export SSH_AUTH_SOCK="/run/user/$UID/gnupg/S.gpg-agent.ssh"

fpath=(~/.zsh/completion $fpath)
autoload -Uz compinit && compinit -i

## disable Ctrl-S
stty -ixon

alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

source ~/.dockeraliases

function vat() {
    VAT=$(echo "$1 * 0.2" | bc)
    VATMIN=$(echo "$1 - $VAT" | bc)
    echo "VAT from $1: $VAT without VAT $VATMIN"
}
function vatplus() {
    VAT=$(echo "$1 * 0.2" | bc)
    VATPLUS=$(echo "$1 + $VAT" | bc)
    echo "VAT from $1: $VAT: incl VAT $VATPLUS"
}

alias glgg="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"
