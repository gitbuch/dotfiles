#
# my bashrc
#

export LANG=en_US.UTF-8

if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi
if [ -f /etc/bash.bashrc ]; then
    . /etc/bash.bashrc
fi
if [ -f /etc/bash_completion ]; then
        . /etc/bash_completion
fi
if [ -f $HOME/.dircolors.rc ]; then
    eval `dircolors -b $HOME/.dircolors.rc`
fi

alias l='ls'
alias sl='ls'
alias la='ls -A'
alias ll='ls -l'
alias lh='ls -lh'
alias lt='ls -lrt'
alias lT='ls -lrta'
alias h=history
alias vi='vim'

# alias ls='ls $LS_OPTIONS'
alias j=jobs
alias pd='pushd'
alias pop='popd'
alias ns='watch netstat -t'

alias lS='ls -lSr'
alias gvim='gvim -rv'
alias ..='cd ..'
alias ...='cd .. ; cd ..'
alias uskb='setxkbmap us intl'
alias atkb='setxkbmap de intl'

source ~/.dockeraliases



### Command history
export HISTCONTROL=ignoreboth
shopt -s cmdhist

export IGNOREEOF=1
export EDITOR='vi'
export BROWSER='firefox'

### TTY appearance
shopt -s checkwinsize

export LESS='-si -x4 -q -f'
export PAGER=less
export LS_OPTIONS='--color=tty -F -b -T 0 -N'

export TEXEDIT="vim +%d %s"

# export CVSROOT=$HOME/CVS
# export CVS_RSH=ssh
# export CVSINGORE="*~|*.class"

export BC_ENV_ARGS=$HOME/.bcrc
export SSH_AUTH_SOCK=~/.gnupg/S.gpg-agent.ssh
## export GREP_OPTIONS='--color=auto --exclude-dir=\.svn --exclude-dir=doc'
export GREP_COLOR='1;33'
export INPUTRC=$HOME/.inputrc
set -o vi


## some useful functions

rot13()
{
	if [ $# = 0 ] ; then
		tr "[a-m][n-z][A-M][N-Z]" "[n-z][a-m][N-Z][A-M]"
	else
		tr "[a-m][n-z][A-M][N-Z]" "[n-z][a-m][N-Z][A-M]" < $1
	fi
}


range () {
    if [ $1 -ge $2 ]; then
        return
    fi
    a=$1
    b=$2
    while [ $a -le $b ]; do
        echo $a;
        a=$(($a+1));
    done
}

# convert an integer to hex using bc:
inttohex() {
    echo $(echo "obase=16; $1" | bc)
}
hextoint () {
    echo $(echo "ibase=16; $1" | bc)
}

# credit goes to:
# http://www.unix.com/tips-tutorials/31944-simple-date-time-calulation-bash.html
stamp2date() {
  date --utc --date "1970-01-01 $1 sec" "+%Y-%m-%d %T"
}
date2stamp () {
  date --utc --date "$1" +%s
}

