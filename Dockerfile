FROM ubuntu:latest
RUN apt-get update && apt-get install -y \
  vim \
  zsh \
  ca-certificates \
  curl \
  python3 \
  python3-neovim \
  python3-pip \
  less \
  neovim \
  git \
  locales \
  && rm -rf /var/lib/apt/lists/* \
  && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8 \
  && pip3 install --upgrade msgpack
ENV LANG en_US.utf8
RUN useradd -ms /bin/zsh ubuntu
WORKDIR /home/ubuntu
USER ubuntu
RUN mkdir .dotfiles.old \
	&& git clone --bare http://gitlab.com/gitbuch/dotfiles .cfg/ \
	&& mv .bashrc .dotfiles.old \
	&& /usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME checkout \
	&& sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" \
	&& mv .zshrc.pre-oh-my-zsh .zshrc \
	&& /usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME submodule update --init # cachebust
CMD ["/bin/zsh", "--login"]
